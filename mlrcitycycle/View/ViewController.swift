//
//  ViewController.swift
//  mlrcitycycle
//
//  Created by Marcus Lindemann Rohden on 10/6/18.
//  Copyright © 2018 Marcus Lindemann Rohden. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    
    var bikeStationsList = [MRBikeStation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Call API and populate list of bike stations
        MRConnection.shared.callApi { (jsonResult,request, response, result) in
            self.populateBikeStationList(tempArray: jsonResult as! [AnyObject])
            print(self.bikeStationsList)
        }
    }
    
    
    func createBikeStation(notParsedBike: AnyObject) -> MRBikeStation{
        let newBikeStation = MRBikeStation()
        newBikeStation.nameOfStation = notParsedBike["name"] as! NSString
        newBikeStation.numberTotalBikeStands = notParsedBike["bike_stands"] as! NSInteger
        newBikeStation.numberAvaiableBikesToTake = notParsedBike["available_bikes"] as! NSInteger
        newBikeStation.numberStandsAvaiable  = notParsedBike["available_bike_stands"] as! NSInteger
        newBikeStation.cityName = notParsedBike["contract_name"] as! NSString
        newBikeStation.lastUpdated = notParsedBike["last_update"]  as! NSInteger
        newBikeStation.stationId = notParsedBike["number"]  as! NSInteger
        newBikeStation.status = notParsedBike["status"]  as! NSString
        let coords = notParsedBike["position"] as! NSDictionary
        let lat = coords["lat"]
        let lng = coords["lng"]
        let tempLocation = CLLocation(latitude: lat as! CLLocationDegrees, longitude: lng as! CLLocationDegrees)
        newBikeStation.coords = tempLocation
        return newBikeStation
    }
    
    func addToBikeStandList(tempBikeStand: MRBikeStation){
        bikeStationsList.append(tempBikeStand)
    }
    
    func populateBikeStationList(tempArray: [AnyObject]){
        var tempBikeStation: MRBikeStation
        for each in tempArray{
            tempBikeStation = createBikeStation(notParsedBike: each)
            addToBikeStandList(tempBikeStand: tempBikeStation)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

