//
//  MRBikeStation.swift
//  mlrcitycycle
//
//  Created by Marcus Lindemann Rohden on 11/6/18.
//  Copyright © 2018 Marcus Lindemann Rohden. All rights reserved.
//

import UIKit
import CoreLocation

class MRBikeStation: NSObject {
    
    var nameOfStation: NSString
    var numberTotalBikeStands: NSInteger
    var numberAvaiableBikesToTake: NSInteger
    var numberStandsAvaiable: NSInteger
    var cityName: NSString
    var lastUpdated: NSInteger
    var stationId: NSInteger
    var coords: CLLocation?
    var status: NSString
    
    
    override init(){
        nameOfStation = "No Station Name"
        numberTotalBikeStands = -1
        numberAvaiableBikesToTake = -1
        numberStandsAvaiable = -1
        cityName = "No City Name"
        lastUpdated = -1
        stationId = -1
        status = "Status not avaiable"
        coords = nil
    }
    
}
