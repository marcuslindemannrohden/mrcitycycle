//
//  MRConnection.swift
//  mlrcitycycle
//
//  Created by Marcus Lindemann Rohden on 10/6/18.
//  Copyright © 2018 Marcus Lindemann Rohden. All rights reserved.
//

import Alamofire

class MRConnection: NSObject {
    
    static let shared = MRConnection()
    
    var jsonResult : Any?
    
    
//     static func signupWith(userToSignup: EPCurrentUser, completionHandler: (success: Bool, error: NSError?) -> ())
    
    
    
    
    func callApi(completionHandler: @escaping (_ json: Any?,_ request: Any?, _ response: Any?, _ result: Any?) -> ()){
        print("Calling Dynamic Request \n")
        
        Alamofire.request("https://api.jcdecaux.com/vls/v1/stations?contract=brisbane&apiKey=d7bcf230a2bb37fc32d60239097a444fe7c31c30").responseJSON { response in
            let tempRequest = "\n Request: \(String(describing: response.request))"  // original url request
            let tempResponse = "\n Response: \(String(describing: response.response))" // http url response
            let tempResult =  "\n Result: \(response.result)"                       // response serialization result
            
            if let json = response.result.value {
                self.jsonResult = json
            }
            completionHandler(self.jsonResult,tempRequest, tempResponse, tempResult)
        }
        
    }
    
    private override init(){
        self.jsonResult = nil
    }
}
